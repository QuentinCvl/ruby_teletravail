=begin
Création d’un tableau avec les chiffres de 1 à 100. (2 méthodes possibles)
Une fois créer, afficher chaque valeur.
=end

# Creation du tableau avec des valeurs allant de 1 a 100
table = Array(1..100)
# Affiche le tableau
puts table
