# on définie la méthode, avec age en facultatif
def machin first_name, age=nil

  #on verifie si le para age est rempli
  # S'il elle ne l'est pas, ça n'affiche que le prenom
  # S'il est l'est, affiche le prenom et l'age
  age.nil? ? "Je m’apelle #{first_name} " : "Je m’apelle #{first_name} , j’ai #{age}ans"



end

# Je lui envoie des valeurs
puts machin 'quentin',23
puts machin 'bernard'

