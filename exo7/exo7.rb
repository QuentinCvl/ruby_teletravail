def game(heart, hiddenword, word)
  while hiddenword.include?('*') && heart > 0 # Tant qu'il reste des lettre non trouvé et des vies
    puts hiddenword.join(' ') # On affiche le mot caché en faisait en sorte que les caractères s'affiche les un a coté des autres et avec un espaces pour pour de lisibilité
    puts "Choissisez une lettre, il vous reste #{heart} vie(s)"

    #recupere la saisie et on la stock dans une variable
    input = gets.chomp

    word.each_with_index { |letter, index |
      # Si la lettre est dans le mot, on remplace le * par la lettre en question
      input == letter ? hiddenword[index] = word[index] : ''
    }
    # on perd une vie a moins qu'il y a la lettre dans le mot
    unless word.index(input)
      heart -= 1
    end
  end
  # dès qu'il  n'y a plus de lettre caché ou plus de vie : affichage du resultat
  # Si il nous reste des vie alors on a gagne
  # sinon on a perdu, bah oui ...
  heart>0 ? (puts"bravo vous avez trouvé le mot #{word.join('')}, il vous restait #{heart} vie(s) !") : (puts "Perdu ! le mot à trouver etait #{word.join('')}")
end

def pendu
  #liste de mot à trouver
  wordList = %w(banjo barbe bonne bruit buche cache capot carte chien crâne cycle essai gifle angora animal arcade aviron azimut babine balade basson billet bouche boucle bronze cabane cloche)

  #on stock un mot tiré au hasard dans une variable
  word = wordList.shuffle[0].split('')
  # Création d'un tableau qui va contenir le mot en caché, chaque lettre remplacé par le symbole *
  hiddenword = []
  word.each { hiddenword.push('*') }

  heart = 6

  game(heart, hiddenword, word)

end

pendu