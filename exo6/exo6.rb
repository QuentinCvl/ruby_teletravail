def verif(input)
  until /^[a-z ]*$/.match(input) do
    puts 'Utilisez uniquement des lettres minuscule et remplacez les - par des espaces'
    input = gets.chomp.gsub(' ', "")
  end
  input
end

def settonumber(input)
  input.tr('abcdefghijklmnopqrstuvwxyz', '12345678912345678922345678')
end


puts 'Entrez votre nom et votre prénom'
input = gets.chomp.gsub(' ', "")
input = verif(input)

input = settonumber(input).split('')
weight = 0
input.each do |n|
  weight += n.to_i
end

while weight >= 10
  table = weight.to_s.split('')
  weight = 0
  table.each do |n|
    weight += n.to_i
  end
end

puts "Le poids de votre nom & prénom est de : #{weight}"





